/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

if (window.location.pathname === "/lab-9/" || window.location.pathname === "/lab-9/cookie/login/") {
	$("#logout").remove();
}

if (window.location.pathname === "/lab-9/cookie/profile/"){
	$("#logout").attr("href", "/lab-9/cookie/clear/")
}