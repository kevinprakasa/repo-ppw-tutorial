
    // This is called with the results from from FB.getLoginStatus().
    // FB initiation function
    window.fbAsyncInit = () => {
      FB.init({
        appId      : '2052342651718906',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.11'
      });
      FB.AppEvents.logPageView();   
      // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
      // dan jalankanlah fungsi render di bawah, dengan parameter true jika
      // status login terkoneksi (connected)

      FB.getLoginStatus(function(response) {
        render(response);
      });
      // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
      // otomatis akan ditampilkan view sudah login
    };

      // Call init facebook. default dari facebook
      (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));

      // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
      // merender atau membuat tampilan html untuk yang sudah login atau belum
      // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
      // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
      const render = loginFlag => {
        if (loginFlag.status === 'connected') {
          // Jika yang akan dirender adalah tampilan sudah login

          // Memanggil method getUserFeedata (lihat ke bawah) yang Anda implementasi dengan fungsi callback
          // yang menerima object user sebagai parameter.
          // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
          getUserData(user => {
          // Render tampilan profil, form input post, tombol post status, dan tombol logout
          // user.picture.data.url
          $('#lab8').html(
            '<div class="photoContainer" style="background-image: url('+user.cover.source+')">' +
                '<div class="item1"> '+
                  '<img class="picture" src="https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/14938250_1536952609665263_8331465695488377642_n.jpg?oh=c5fe8fb987ad200948812d950d71ccbd&oe=5A8FA05E" alt="profpic" />' +
                  '<span class="userName">' + user.name + '</span>' +
                '</div>' +              
                '<div class="item2">' +
                    '<b>About me</b>' +
                    '<div>'+user.about+'</div>'+
                    '<b>E-mail</b>' +
                    '<div>'+user.email+'</div>'+
                    '<b>Gender</b>' +
                    '<div>'+user.gender+'</div>'+
                '</div>' +
            '</div>' +
            '<div class="postFeed">'+
              '<textarea id="postInput" class="post" placeholder="Ketik Status Anda" />' +
              '<button class="postStatus" onclick="postStatus()">Post Status</button>' +
              '<button class="logout" onclick="facebookLogout()">Logout</button>'+
            '</div>'
            );

          // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
          // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
          // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
          // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
          getUserFeed(feed => {
            feed.data.map(value => {
              // Render feed, kustomisasi sesuai kebutuhan.
              if (value.message && value.story) {
                $('#status').append(
                  '<div class="feed">' +
                                    '<div class="smallPic"> <img src="'+user.picture.data.url+'" alt="smallpic"/>'+user.name+'</div>'+
                  '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="'+value.id+'" value="Delete this post" ></input>'+
                  '<div>' + value.message + '</div>' +
                  '</div>'
                  );
              } else if (value.message) {
                $('#status').append(
                  '<div class="feed">' +
                  '<div class="smallPic"> <img src="'+user.picture.data.url+'" alt="smallpic"/>'+user.name+'</div>'+
                  '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="'+value.id+'" value="Delete this post" ></input>'+
                  '<div>' + value.message + '</div>' +
                  '</div>'
                  );
              } else if (value.story) {
                $('#status').append(
                  '<div class="feed">' +
                  '<div class="smallPic"> <img src="'+user.picture.data.url+'" alt="smallpic"/>'+user.name+'</div>'+
                  '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="'+value.id+'" value="Delete this post"></input>'+
                  '<div>' + value.story + '</div>' +
                  '</div>'
                  );
              }
            });
        });
    });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html(
        '<div class="welcomeText"><span id="noEnter1">You haven\'t signed in yet?</span><br><span id="noEnter"> Do it then.</span></div>'+'<button href="#" onclick="facebookLogin()" class="login">Login</button>' 
        );
      $('#status').empty();
    } 
  };

  const facebookLogin = () => {
      // TODO: Implement Method Ini
      FB.login(function(response) {
        // handle the response
        render(response);
      }, {scope: 'public_profile,user_posts,publish_actions,user_about_me,user_posts'});
      // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
      // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
      // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    };

    const facebookLogout = () => {
      // TODO: Implement Method Ini
        FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            FB.logout();
            console.log('logout success');  
            render("failed");
          } 
       });

      // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
      // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    };

      // TODO: Lengkapi Method Ini
      // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
      // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
      // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
      // meneruskan response yang didapat ke fungsi callback tersebut
      // Apakah yang dimaksud dengan fungsi callback?
    const getUserData = (fun) => {
      FB.api('/me?fields=id,name,cover,picture,email,gender,address,about', 'GET', function (response){
        fun(response);
      });
    };

      const getUserFeed = (fun) => {
      // TODO: Implement Method Ini
      FB.api(
          "/me/feed",
          function (response) {
            if (response && !response.error) {
              fun(response);
              /* handle the result */
            }
          }
      );
      // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
      // yang sedang login dengan sedangmua fields yang dibutuhkan di method render, dan memanggil fungsi callback
      // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
      // tersebut
    };

    const deleteFeed = (e) => {
      FB.api(
          "/"+e.target.id+"",
          "DELETE",
          function (response) {
            if (response && !response.error) {
              alert('This post has deleted');
              e.target.parentNode.style.display = 'none';
            }
          }
      );
    };

    const postFeed = (message) => {
      // Todo: Implement method ini,
      FB.api(
          "/me/feed",
          "POST",
          {
              "message": message
          },
          function (response) {
            if (response && !response.error) {
              /* handle the result */
              getUserData( (user) =>{
                console.log(user);
                $('#status').prepend(
                  '<div class="feed">' +
                  '<div class="smallPic"> <img src="'+user.picture.data.url+'" alt="smallpic"/>'+user.name+'</div>'+
                  '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="'+response.id+'" value="Delete this post" ></input>'+
                  '<div>' + message + '</div>' +
                  '</div>'
                  );
              }
                );
            }
          }
      );
      // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
      // Melalui API Facebook dengan message yang diterima dari parameter.
    };

    const postStatus = () => {
      const message = $('#postInput').val();
      postFeed(message);
    };