# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from .views import index, friend_list, model_to_dict
# from .models import Friend

# class Lab7UnitTest(TestCase):
# 	def  test_lab_7_url_is_exist(self):
# 		 response = Client().get('/lab-7/')
# 		 self.assertEqual(response.status_code, 200)

# 	def test_lab_7_using_index_func(self):
# 		found = resolve('/lab-7/')
# 		self.assertEqual(found.func, index)

# 	def test_model_can_create_new_friend(self):
# 		new_friend = Friend.objects.create(friend_name="kape", npm="1606917777")
# 		counting_all_available_friend = Friend.objects.all().count()
# 		self.assertEqual(counting_all_available_friend,1)

# 	def test_lab_7_using_friend_list_func(self):
# 		found = resolve('/lab-7/get-friend-list/')
# 		self.assertEqual(found.func, friend_list)

# 	def test_lab_7_using_friend_list(self):
# 		response = Client().get('/lab-7/get-friend-list/')
# 		self.assertEqual(response.status_code, 200)

# 	def test_lab_7_add_friend_func(self):
# 		test = 'kape'
# 		response = Client().post('/lab-7/add-friend/', {'name': test, 'npm': '1111111'})
# 		self.assertEqual(response.status_code, 200)

# 	def test_lab_7_get_friends(self):
# 		response = Client().get('/lab-7/get-friends/')
# 		self.assertEqual(dict, type(response.json()))

# 	def test_lab_7_delete_friend(self):
# 		new_friend = Friend.objects.create(friend_name='Dummy', npm='09877654321')
# 		response = Client().post('/lab-7/delete-friend/', {'friend_id':new_friend.id})
# 		self.assertEqual(response.status_code, 200)
	
# 	def test_lab_7_validate_npm(self):
# 		response = Client().post('/lab-7/validate-npm/', {'npm':'1111111'})
# 		self.assertEqual(response.json()["is_taken"], False)

# 	def test_lab_7_insert_old_friend(self):
# 		new_friend = Friend.objects.create(friend_name='hohoho', npm='1234567')
# 		response = Client().post('/lab-7/add-friend/', {'name':new_friend.friend_name, 'npm': new_friend.npm})
# 		self.assertEqual(response.status_code, 404)