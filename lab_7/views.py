from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.forms.models import model_to_dict
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
from api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    paginator = Paginator(mahasiswa_list, 10) 
    page = request.GET.get('page')
    try:
        mahasiswa = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        mahasiswa = paginator.page(1)
    # except EmptyPage:
    #     # If page is out of range (e.g. 9999), deliver last page of results.
    #     mahasiswa = paginator.page(paginator.num_pages)
    friend_list = Friend.objects.all()
    response = {"mahasiswa": mahasiswa, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        if (Friend.objects.filter(npm=npm).exists()) :
            raise Http404("Friend is existed")
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        print(data)
        return JsonResponse(data)

def get_friends(request):
	friends = Friend.objects.all().values('id','friend_name','npm')
	print(friends)
	return JsonResponse({"data":list(friends)})

@csrf_exempt
def delete_friend(request):
	if request.method == 'POST':
		friend_id = request.POST['friend_id'] 
		Friend.objects.filter(id=friend_id).delete()
		response = {'status': 1, 'message':"Ok","url": "/lab-7"}
		return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    is_taken = Friend.objects.filter(npm=npm).exists()	
    data = {
        'is_taken': is_taken #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

