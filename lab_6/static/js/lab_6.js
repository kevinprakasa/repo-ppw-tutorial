function onTestChange() {
    var key = window.event.keyCode;
    // If the user has pressed enter
    if (key === 13) {
      const message = $('textarea').val();
      let bubble;
      console.log(Math.floor(Math.random() * (1 - 0 + 1)) + 0);
      if(Math.random()*2 > 1 ){
        bubble = $('<div class="msg-receive"></div>').text(message);
      } else {
        bubble = $('<div class="msg-send"></div>').text(message);
      }
      $('.msg-insert').append(bubble);
      $('textarea').val('');
      event.preventDefault();
    }
}
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }else if (x === 'log'){
    print.value = Math.log(print.value);
  }else if (x === 'sin'){
    print.value = Math.sin(print.value);
  }else if (x === 'tan'){
    print.value = Math.tan(print.value);
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
$(document).ready(function() {
  const selectValue = JSON.stringify([
  {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
  ]);
;
if (localStorage.selectedTheme) {
  const selectedTheme = JSON.parse(localStorage.selectedTheme);
  var body = document.getElementsByTagName("BODY")[0];
  console.log(selectedTheme);
  for (obj in selectedTheme) {
    body.style.background = selectedTheme[obj].bcgColor;
    document.getElementsByClassName('text-center')[0].style.color = selectedTheme[obj].fontColor;
  }
}

  if(typeof(Storage) !== "undefined") {
    localStorage.themes = selectValue;
  } else {
     document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
  }
  const themes = JSON.parse(localStorage.themes);
    $('.my-select').select2({
      'data':themes
    });
    $('#applyButton').on('click', function(){  // sesuaikan class button
      // [TODO] ambil value dari elemen select .my-select

      const selectedOption = $('option:selected').text();
      // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada

      var result = $.grep(themes, function(e){ return e.text === selectedOption; })[0];
      // [TODO] ambil object theme yang dipilih
      // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya

      var body = document.getElementsByTagName("BODY")[0];
      body.style.background = result.bcgColor;
      document.getElementsByClassName('text-center')[0].style.color = result.fontColor;

      // [TODO] simpan object theme tadi ke local storage
      const selectedTheme = {};
      selectedTheme[result.text] = {"bcgColor":result.bcgColor,"fontColor":result.fontColor};
      localStorage.selectedTheme = JSON.stringify(selectedTheme);

    });

});
